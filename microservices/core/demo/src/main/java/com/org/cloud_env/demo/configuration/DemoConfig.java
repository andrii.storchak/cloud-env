package com.org.cloud_env.demo.configuration;


import com.org.cloud_env.common.configuration.AddSwaggerConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Slf4j
@Configuration
@AddSwaggerConfig
@EnableEurekaClient
public class DemoConfig {
}

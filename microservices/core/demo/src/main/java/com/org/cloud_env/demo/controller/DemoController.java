package com.org.cloud_env.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
@Validated
@RequestMapping(produces = APPLICATION_JSON_UTF8_VALUE)
@Slf4j
public class DemoController {

    @GetMapping("/boo")
    public String train() {
        log.info("boo");
        return "{boo:1}";
    }
}

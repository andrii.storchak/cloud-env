package com.org.cloud_env.gateway.security.exception;

import org.springframework.security.authentication.AuthenticationServiceException;

public class AuthException extends AuthenticationServiceException {

    public AuthException(String s) {
        super(s);
    }

}

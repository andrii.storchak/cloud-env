package com.org.cloud_env.gateway.security.filter;

import com.org.cloud_env.gateway.security.AuthToken;
import com.org.cloud_env.gateway.security.exception.AuthException;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Setter
@Slf4j
public class AuthFilter extends AbstractAuthenticationProcessingFilter {

    public AuthFilter(RequestMatcher requiresAuthenticationRequestMatcher) {
        super(requiresAuthenticationRequestMatcher);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException {
        String sid = Optional.ofNullable(httpServletRequest.getHeader("session"))
                .orElseThrow(() -> new AuthException("Session is missing"));
        String uid = Optional.ofNullable(httpServletRequest.getHeader("uid"))
                .orElseThrow(() -> new AuthException("uid is missing"));
        return getAuthenticationManager()
                .authenticate(new AuthToken(sid, uid, httpServletRequest.getServletPath()));
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);
        chain.doFilter(request, response);
    }
}

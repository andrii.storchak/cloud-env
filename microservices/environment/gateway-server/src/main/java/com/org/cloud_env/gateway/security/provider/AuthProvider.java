package com.org.cloud_env.gateway.security.provider;

import brave.Tracer;
import com.org.cloud_env.gateway.configuration.WebSecurityConfig;
import com.org.cloud_env.gateway.configuration.props.ConfigProperties;
import com.org.cloud_env.gateway.security.AuthToken;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class AuthProvider implements AuthenticationProvider {
    @Setter(onMethod = @__({@Autowired}))
    private ConfigProperties configProperties;
    @Setter(onMethod = @__({@Autowired}))
    private Tracer tracer;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        AuthToken authToken = (AuthToken) authentication;
        MDC.put("sid", authToken.getSession());
        MDC.put("uid", authToken.getUid());
        log.info("Authenticate chameleonSession [{}]", authToken.getSession());

        return AuthToken.builder()
                .session(authToken.getSession())
                .uid(authToken.getUid())
                .context(authToken.getContext())
                .authorities(AuthorityUtils.commaSeparatedStringToAuthorityList(WebSecurityConfig.USER_ROLE))
                .build();
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return (AuthToken.class.isAssignableFrom(aClass));
    }
}

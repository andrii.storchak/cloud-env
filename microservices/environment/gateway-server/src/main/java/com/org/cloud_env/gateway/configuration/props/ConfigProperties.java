package com.org.cloud_env.gateway.configuration.props;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class ConfigProperties {
    @Value("${auth.host}")
    private String authHost;
}

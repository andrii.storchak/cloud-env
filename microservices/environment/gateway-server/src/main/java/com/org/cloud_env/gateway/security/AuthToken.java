package com.org.cloud_env.gateway.security;

import lombok.Builder;
import lombok.Data;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Collections;


@Data
public class AuthToken extends AbstractAuthenticationToken {
    private String session;
    private String uid;
    private String context;

    public AuthToken() {
        super(Collections.emptyList());
        this.setAuthenticated(false);
    }

    @Builder
    public AuthToken(String session, String uid, String context) {
        super(Collections.emptyList());
        this.setAuthenticated(false);
        this.context = context;
        this.uid = uid;
        this.session = session;
    }

    @Builder
    public AuthToken(String session, String uid, String context, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.setAuthenticated(true);
        this.session = session;
        this.context = context;
        this.uid = uid;
    }

    public Object getCredentials() {
        return "";
    }

    public Object getPrincipal() {
        return this.session;
    }

}

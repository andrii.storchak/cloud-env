package com.org.cloud_env.gateway.configuration;

import com.org.cloud_env.gateway.security.filter.AuthFilter;
import com.org.cloud_env.gateway.security.handler.ApiAuthenticationEntryPoint;
import com.org.cloud_env.gateway.security.handler.AuthFailureHandler;
import com.org.cloud_env.gateway.security.handler.AuthSuccessHandler;
import com.org.cloud_env.gateway.security.provider.AuthProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.session.NullAuthenticatedSessionStrategy;
import org.springframework.security.web.savedrequest.NullRequestCache;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    public static final String USER_ROLE = "USER_GRANTED";
    private static final String[] AUTH_WHITELIST = {
            "/**/swagger**",
            "/**/swagger-resources/**",
            "/**/webjars/**",
            "/**/v2/**",
            "/**/util/**",
            "/**/actuator/**"
    };

    @Autowired
    private AuthSuccessHandler successHandler;
    @Autowired
    private AuthFailureHandler failureHandler;
    @Autowired
    private ApiAuthenticationEntryPoint apiAuthenticationEntryPoint;
    @Autowired
    private AuthProvider authProvider;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    private AuthFilter buildAuthFilter() throws Exception {
        AuthFilter jsonAuthFilter =
                new AuthFilter(new NegatedRequestMatcher(new OrRequestMatcher(Stream.of(AUTH_WHITELIST)
                        .map(AntPathRequestMatcher::new)
                        .collect(Collectors.toList()))));
        jsonAuthFilter.setAllowSessionCreation(false);
        jsonAuthFilter.setSessionAuthenticationStrategy(new NullAuthenticatedSessionStrategy());
        return (AuthFilter) buildAuthFilterBehavior(jsonAuthFilter);
    }

    private AbstractAuthenticationProcessingFilter buildAuthFilterBehavior(AbstractAuthenticationProcessingFilter filter) throws Exception {
        filter.setAuthenticationManager(super.authenticationManagerBean());
        filter.setAuthenticationSuccessHandler(successHandler);
        filter.setAuthenticationFailureHandler(failureHandler);
        return filter;
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .exceptionHandling()
                .authenticationEntryPoint(apiAuthenticationEntryPoint)
                .and()
                .authorizeRequests()
                .antMatchers("/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .requestCache()
                .requestCache(new NullRequestCache())
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .csrf().disable()
                .addFilterAfter(buildAuthFilter(), UsernamePasswordAuthenticationFilter.class);
    }

}

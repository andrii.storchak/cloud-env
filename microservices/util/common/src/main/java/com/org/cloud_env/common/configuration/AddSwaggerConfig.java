package com.org.cloud_env.common.configuration;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(SwaggerConfig.class)
public @interface AddSwaggerConfig {
}

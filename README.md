# Cloud Env


This project shows how to work with Spring Cloud stack (config server, discovery, gateway) and provides an example of 
authorization on gateway server settings. Also you can an example of packing applications to docker image.